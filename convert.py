import numpy as np
from PIL import Image
from pprint import pprint
import math


'''
divisor of 360
'''
divisor = [
    360,
    180,
    120,
    90,
    72,
    60,
    45,
    40,
    36,
    30,
    24,
    20,
    18,
    15,
    12,
    10,
    9,
    8,
    #6,
    #5,
    #4,
    #3,
    #2,
    1
]


# image source
url = './assets/color2.png'

# 
factor = .1

pixelMap = []
pixelMapRowStart = [0]
pixelMapRowC = []

with Image.open( url ) as originalImage:
    w, h = originalImage.size
    print('--- ORIGINAL SIZE', w, h)

    xEdge = np.mod( w, 360) # cut off fragment if not integer multiple of 360
    yEdge = np.mod( h, 180 ) # cut off fragment if not integer multiple of 180

    # max realistic factor
    maxFactorX = int( np.divide( w, 360 ) )
    maxFactorY = int( np.divide( h, 180 ) )
    print('--- MAX FACTORS', maxFactorX, maxFactorY)

    if factor > maxFactorX:
        factor = maxFactorX

    left = int( np.divide( xEdge, 2 ) )
    upper = int( np.divide( yEdge, 2 ) )
    right = np.sum( ( left, np.multiply( maxFactorX, 360 ) ) )
    lower = np.sum( ( upper, np.multiply( maxFactorY, 180) ) )

    #print('1', left, upper, right, lower)

    # crop centered to multiple of 360 (width) or 180 (height)
    croppedImage = originalImage.crop( ( left, upper, right, lower ) )
    #croppedImage.save('./assets/crop.jpg')

    w, h = croppedImage.size
    print('--- CROP SIZE', w, h)

    # aspect ratio of cropped image
    ratio = np.divide( w, h )

    left = 0
    upper = 0
    newW = w
    newH = h

    if ratio > 2: # crop width
        newW = np.multiply( h, 2 )
        left = int( np.divide( np.subtract( w, newW), 2 ) )
    elif ratio < 2: # crop height
        newH = int( np.divide( w, 2 ) )
        upper = int( np.divide( np.subtract( h, newH), 2 ) )

    right = np.sum( ( left, newW ) )
    lower = np.sum( ( upper, newH ) )
    #print('map', w, h, left, upper, right, lower )


    mappingImage = croppedImage.crop( ( left, upper, right, lower ) )
    #mappingImage.save('./assets/out.jpg')

    w, h = mappingImage.size
    print('--- MAPPING SIZE', w, h)
    
    stepSizeMin = int( np.divide( w, np.multiply( 360, factor ) ) )

    stepsPer90 = np.divide( np.divide( h, 2 ), stepSizeMin ) # steps per 90 degrees

    # max steps per hemisphere
    maxLevel = int(9 if stepsPer90 > 9 else stepsPer90)

    changeAfterSteps = np.divide( stepsPer90, maxLevel )

    stepSize = stepSizeMin

    print('--- FACTOR', factor)
    print('--- STEP SIZE', stepSize)

    y = 0 # int( np.divide( stepSize, 2 ) )

    level = -maxLevel
    levelChangeCount = 0
    loopCount = 0
    allCount  = 0
    maxXCount = 0

    # reduce image to color map 

    # step from northpole to southpole by specific latitude angle
    while y <= h-1:
        #print('--- LEVEL', np.abs(level), divisor[np.abs(level)])
        #print(np.abs(90-int(y/(h/2)*90)), level)
        stepSizeX = int( np.divide( w, np.multiply( divisor[np.abs(level)], factor ) ) ) # use level value as aditional factor
        if stepSizeX > w:
            stepSizeX = w
        x = int( np.divide( stepSizeX, 2 ) )
        xCount = 0
        pixelMap.append([]) # TODO convert to tuples for arduino

        # step through longitude angle and get color
        while x <= w-1:
            print('--- POS', x, y)
            pixelColor = mappingImage.getpixel( ( int( x ), int( y ) ) )
            pixelMap[loopCount].append( pixelColor )
            pixelMapRowC.append('{newline}RgbColor{pColor}'.format(pColor = pixelColor[:3], newline = '\n' if xCount == 0 and y != 0 else ''))
            x += stepSizeX
            xCount += 1
            allCount += 1

        if maxXCount < xCount:
            maxXCount = xCount

        if y < h-1 and y+stepSizeMin > h-1:
            y = h-1
        else:
            y += stepSizeMin

        pixelMapRowStart.append(pixelMapRowStart[-1] + xCount)

        loopCount += 1
        levelChangeCount += 1
        if levelChangeCount == changeAfterSteps:
            print('--- LEVEL', np.abs(level), xCount)
            levelChangeCount = 0
            level += 1

    print('--- LOOPS', loopCount, int(x-stepSizeX/2), y)

    # output of pixel map to file
    with open('./assets/map.txt', 'w') as map_file:
        map_file.write(str(pixelMap))

    pixelMapRowStartStr = '{{ {pmrs} }}'.format(pmrs = ', '.join(map(str, pixelMapRowStart)) )
    with open('./assets/mapC.txt', 'w') as map_file:
        map_file.write('{{\n{{ {pmc}\n}},\n {pmrs}\n}}'.format(pmc = ', '.join(pixelMapRowC), pmrs = pixelMapRowStartStr))
    
    #pprint(pixelMap)
    print(allCount)

    print((maxXCount, len(pixelMap)))

    # save color map to image to visually verify result
    out = Image.new('RGB', (maxXCount, len(pixelMap)))

    imageMap = []
    y = 0
    for row in pixelMap: # for every row set pixels
        transPixelCount = maxXCount - len(row)
        colCount = 0
        imageMap.append([])
        #print('Y', y)

        for col in range(0, round(transPixelCount/2)):
            #print('X', colCount)
            out.putpixel((colCount, y), (255, 255, 255) ) # at row, col
            imageMap[y].append((255, 255, 255))
            colCount += 1

        for col in row:
            #print('X', colCount)
            out.putpixel((colCount, y), col ) # at row, col
            imageMap[y].append(col)
            colCount += 1

        for col in range(colCount, maxXCount):
            #print('X', colCount)
            out.putpixel((colCount, y), (255, 255, 255) ) # at row, col
            imageMap[y].append((255, 255, 255))
            colCount += 1
        
        #pprint(imageMap[y])
        y += 1

    out.save('./assets/color2_map.png')