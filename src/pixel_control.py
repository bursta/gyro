import math


class PixelControl:
    """
    Calculate the colors of light dots on the centered bar

    :param matrix: pixel map for rows with columns of colors
    :param ledCount: amount of leds in the dot strip
    :param gearRatio: gear ratio of 2. rotation level to 1. level

    """

    map = []
    rowCount = 0
    gearRatio = 3
    ledCount = 2

    def __init__(self, matrix, ledCount=2,  gearRatio=3):
        self.map = matrix
        self.rowCount = len(self.map)
        self.segmentAngle = 180 / (self.rowCount-1)
        self.gearRatio = gearRatio
        self.ledCount = ledCount
        self.ledAngle = 360 / self.ledCount # angle between the existing leds
        #print(self.rowCount, self.gearRatio, self.segmentAngle, self.ledAngle)

    # get all leds colors by 1. level rotation angle
    def getColors(self, angle):
        colors = []
        
        angleYZ = (angle * self.gearRatio) % 360

        backSide = False
        if angleYZ < 180: # north pole is first row (top)
            backSide = True

        angleRow = round((180 - (angleYZ - 180) if not backSide else angleYZ) / self.segmentAngle)

        row = self.map[angleRow]
        colCount = len(row)

        column = int(( ( (angle + (180 if backSide else 0)) % 360) / 360) * colCount)
        # master led 
        colors.append(row[column])

        #print(' --- AngleXY', ( (angle + (180 if backSide else 0)) % 360), 'Col', column, 'ColCount', colCount, 'AngleYZ', angleYZ, 'Row',angleRow, backSide, '=', row[column])

        # custom further leds with specific offset
        for _ in range(1, self.ledCount):
            angleYZ = (angleYZ + self.ledAngle) % 360

            backSide = False
            if angleYZ < 180:
                backSide = True
            
            angleRow = round((180 - (angleYZ - 180) if not backSide else angleYZ) / self.segmentAngle)
            
            row = self.map[angleRow]
            colCount = len(row)

            column = int(( ( (angle + (180 if backSide else 0)) % 360) / 360) * colCount)
            colors.append(row[column])

            #print(' --- AngleXY', ( (angle + (180 if backSide else 0)) % 360), 'Col', column, 'ColCount', colCount, 'AngleYZ', angleYZ, 'Row',angleRow, backSide, '=', row[column])

        #row = self.map[self.rowCount-1-angleRow]
        #colors.append(row[colCount-1-column]) # seconds light rotated by 180 degrees
        #print('AngleXY', (angle+180)%360, 'Col', colCount-1-column, 'AngleYZ', (angleYZ+180)%360, 'Row',self.rowCount-1-angleRow, '=', row[colCount-1-column])

        return colors

'''
import time
from pixel_map import pixelmap

control = PixelControl(pixelmap, 2)

lastTime = time.time_ns()
loopCount = 0
startTime = time.time_ns()
while loopCount <= 1080:
    currentTime = time.time_ns()

    #if currentTime - lastTime > 1:
    colors = control.getColors(loopCount)
    #print(loopCount, colors)
    #print()
    loopCount += 1
    lastTime = currentTime

print('TIME PER COLOR', (time.time_ns() - startTime) /1000 / 1080)

# 0.9999930747922438 milliseconds ???
'''