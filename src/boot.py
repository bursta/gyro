# This file is executed on every boot (including wake-boot from deepsleep)
#import esp
#esp.osdebug(None)
#import webrepl
#webrepl.start()


import network
from credentials import SSID, PASSWORD, PORT

try:
    import usocket as socket
except:
    import socket


# init wifi connection
def do_connect(ssid, pwd):
    connected = False
    wlan = network.WLAN(network.STA_IF)
    #wlan.scan()    # Scan for available access points

    if not wlan.isconnected() or not connected:
        print('Connecting to network "%s"...' % SSID)

        wlan.active(True)
        wlan.config(dhcp_hostname='esp32-pov')
        wlan.connect(ssid, pwd)

        while not wlan.isconnected():
          pass
    
    connected = True
    print('NETWORK: IP %s | Netmask %s | Gateway %s | Nameserver %s' % wlan.ifconfig())
    return connected


# start web server on specific port
def start_server(port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('', port))
    s.listen(5)

    return s


# server object for main.py
server = None

if do_connect(SSID, PASSWORD):
    server = start_server(PORT)