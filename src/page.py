import json
import esp32

# gyro control page; with button to activate and possibility to change 2. level angle and 1. level speed
def home(state, data):
    html = '''
        <!DOCTYPE html>
        <html lang="en">
            <head>
                <title>Gyro Control</title>
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta charset="UTF-8">
                <link rel="icon" href="https://spezi.duckdns.org:9877/favicon.ico">
                <link rel="stylesheet" type="text/css" href="https://cdn.materialdesignicons.com/6.4.95/css/materialdesignicons.min.css">
                
                <style>
                    html {{
                        font-family: Helvetica;
                        display:inline-block;
                        margin: 0px auto;
                        text-align: center;
                    }}
                    h1 {{
                        margin-top: 0;
                        font-size: 5em;
                    }}
                    p {{
                        font-size: 3rem;
                        padding: 0;
                        margin: 0;
                    }}
                    #rpm, #angle {{
                        vertical-align: bottom;
                    }}
                    .button {{
                        vertical-align: bottom;
                        display: inline-block;
                        background-color: rgba(0,0,0,.5);
                        border: none;
                        border-radius: 4px;
                        color: white;
                        text-decoration: none;
                        cursor: pointer;
                        padding: 2px 5px;
                        font-size: 24px;
                    }}

                    .divTable{{
                        display: table;
                        width: 100%;
                    }}
                    .divTableRow {{
                        display: table-row;
                    }}
                    .divTableCell {{
                        display: table-cell;
                        padding: 3px 10px;
                    }}
                    .divTableBody {{
                        display: table-row-group;
                    }}
                    #active_button {{
                        width: 70px;
                        height: 70px;
                        border-radius: 35px;
                        border: 1px solid rgba(0,0,0,.5);
                        font-size: 50px;
                        background-color: unset;
                    }}
                </style>

                <script>
                    function ajax(key) {{
                        var value = document.getElementById(key).value
                        var queryStr = key + '=' + value

                        var xhr = new XMLHttpRequest()
                        xhr.open('GET', '/?'+queryStr)
                        xhr.onload = function() {{
                            if (xhr.status === 200) {{
                                //console.log('Successfully send '+queryStr)
                                //location.reload()
                                if (key !== 'active') {{
                                    document.getElementById(key+'_value').textContent = value
                                }} else {{
                                    document.getElementById(key).value = value == 1 ? 0 : 1
                                    document.getElementById(key+'_button').style.color = value == 1 ? '#f44336' : '#2196f3'
                                }}
                            }}
                        }}
                        xhr.send()
                    }}
                </script>
            </head>

            <body>
                <div style="margin-top:50px;">
                    <img width="300px" src="https://spezi.duckdns.org:9877/logo_t.svg" />
                </div>
                <h1>Control</h1> 

                 <div>
                    <div>
                        <p>
                            <input type="hidden" id="active" name="active" value="{active}">
                            <button id="active_button" style="color:{activeColor};" onclick="ajax('active')" class="button mdi mdi-power"></button>
                        </p>
                    </div>
                </div>

                <div class="divTable" style="width: 100%;" >
                    <div class="divTableBody">
                        <div class="divTableRow">
                            <div class="divTableCell">
                                <div>
                                    <p>
                                        <span class="mdi mdi-gauge"></span> <strong id="rpm_value">{rpm:.0f}</strong> <strong>RPM</strong>
                                    </p>
                                </div>
                            
                                <div>
                                    <p>
                                        <input type="number" id="rpm" name="rpm" value="{rpm:.0f}">
                                        <button onclick="ajax('rpm')" class="button mdi mdi-content-save"></button>
                                    </p>
                                </div>
                            </div>

                            <div class="divTableCell">
                                <div>
                                    <p>
                                        <span class="mdi mdi-angle-acute"></span> <strong id="angle_value">{angle:.0f}</strong><strong>&deg;</strong>
                                    </p>
                                </div>
                                
                                <div>
                                    <p>
                                        <input type="number" id="angle" name="angle" value="{angle:.0f}">
                                        <button onclick="ajax('angle')" class="button mdi mdi-content-save"></button>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <div>
                        <p>
                            <span class="mdi mdi-thermometer"></span> <strong id="temp_value">{temperature:.2f}</strong><strong>&deg;</strong>
                        </p>
                    </div>
                </div>
            </body>
        </html>'''.format(
            rpm = data['rpm'],
            angle = data['angle'],
            active = (0 if data['active'] == True else 1),
            activeColor = ('#2196f3' if data['active'] == False else '#f44336'),
            temperature = (esp32.raw_temperature() - 32) / 1.8
        )

    return html


def dummy():
    return 'Nothing'