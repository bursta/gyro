# esp32 dimensions
# H x B x L
# 1.3 x 2.6 x 4.9

# ampy -d 1 -p /dev/ttyUSB0  -b 115200 put main.py pixel_control.py dotstar.py

# picocom /dev/ttyUSB0 -b115200

# esptool.py --chip esp32 --port /dev/ttyUSB0 erase_flash
# esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 460800 write_flash -z 0x1000 assets/esp32-20210902-v1.17.bin

# https://www.techcoil.com/blog/how-to-setup-micropython-webrepl-on-your-esp32-development-board/

# https://github.com/mattytrentini/micropython-dotstar

#from sphere import Sphere
from dotstar import DotStar
from pixel_control import PixelControl
from machine import SPI, Pin
import time
from pixel_map import pixelmap
import config
import _thread
import utime
import page


def getConfig():
    config = {'rpm': 0, 'led': 1}

    with open('config.ini', 'r') as configfile:
        variables = configfile.readlines()

        for variable in variables:
            varSplit = variable.split('=')
            key = varSplit[0]
            value = int(varSplit[1])

            config[key] = value

    return config

def setConfig(rpm, led):
    with open('config.ini', 'w') as configfile:
        configfile.write("rpm="+str(rpm)+"\nled="+str(led))


CONFIG = getConfig()

# global variables and constants
ACTIVE = False

ANGLE = CONFIG['angle']
VELOCITY = CONFIG['rpm'] / 60 / 3 / 1000000   # per Second and gear ratio -> real RPS
VELOCITY_DEGREE = VELOCITY * 360
GEAR_RATIO = (90 / 29) * (60 / 20)

spi = SPI(sck=Pin(config.PIN_SCK), mosi=Pin(config.PIN_MOSI), miso=Pin(config.PIN_MISO)) # configure SPI
dotstar = DotStar(spi, CONFIG['led']) # init led strip with spi object and led count

# init leds with red color
for i in range(0, CONFIG['led']):
    dotstar[i] = (255, 0, 0) # Red


# home endpoint is control page
def home():
    global ACTIVE
    data = getConfig()
    data['active'] = ACTIVE
    
    state = True
    html = page.home(state, data)
    content_type = 'text/html'

    return {
        'data': html,
        'type': content_type
    }


# parse web query to dict
def getQuery(path):
    queries = {}
    path_split = path.split('?')

    if len(path_split) == 2:
        query_str = path_split[1]
        query_split = query_str.split('&')

        for query in query_split:
            query_kv = query.split('=')
            if len(query_kv) == 2:
                queries[query_kv[0]] = query_kv[1]

    return queries


# handle query requests by query key
def set_action(route):
    query = getQuery(route)

    if query.get('rpm'):
        global VELOCITY
        global VELOCITY_DEGREE
        setConfig(query['rpm'], 2)
        VELOCITY = int(query['rpm']) / 60 / 3 / 1000000
        VELOCITY_DEGREE = VELOCITY * 360
    elif query.get('angle'):
        global ANGLE
        ANGLE += int(query['angle'])
    elif query.get('active'):
        global ACTIVE
        print(query['active'] == '1')
        ACTIVE = True if int(query['active']) == 1 else False


# response with right route
def route_response(request):
    try:
        request_split = request.decode('utf8').split(' ')
        print(request_split)

        if len(request_split) <= 1:
            #raise Exception('No valid request: "%s"' % request.decode('utf8'))
            #print('No valid request: "%s"' % request.decode('utf8'))
            return None

        route = request_split[1]

        set_action(route)
        
        route = route.split('?')[0]
        
        return {
            '/': home,
        }[route]
    except Exception as e:
        print('ROUTE Error', e)
        return home


# thread to handle web requests
def serverThread(server):
    while server:
        conn, addr = server.accept()
        print('Connection from IP %s on Port %s' % addr)
        request = conn.recv(1024)

        try:
            response = route_response(request)
            
            if response:
                response_data = response()

                conn.send('HTTP/1.1 200 OK\n')
                conn.send('Content-Type: %s\n' % response_data['type'])
                conn.send('Connection: close\n\n')
                conn.sendall(response_data['data'])
        except Exception as e:
            print('SERVER', e)

        conn.close()


_thread.start_new_thread(serverThread, [server])



# thread to handle pixel color change by rotation angle
def pixelThread():
    global ACTIVE
    global ANGLE
    global VELOCITY_DEGREE
    global GEAR_RATIO

    scanGap_us = 1000000
    lastTick_us = utime.ticks_add(utime.ticks_us(), -scanGap_us)
    
    control = PixelControl(pixelmap, gearRatio = GEAR_RATIO)

    while True:
        if ACTIVE:
            currentTick_us = utime.ticks_us()
            currentScanGap_us = utime.ticks_diff(currentTick_us, lastTick_us)
            
            if currentScanGap_us >= scanGap_us:
                # calc angle by expired time
                ANGLE += (VELOCITY_DEGREE * currentScanGap_us)
                colors = control.getColors(ANGLE)
                
                for idx, color in enumerate(colors):
                    dotstar[idx] = color
                #print(colors)
                lastTick_us = utime.ticks_us()


_thread.start_new_thread(pixelThread, ())


'''
spi = SPI(sck=Pin(14), mosi=Pin(13), miso=Pin(12)) # Configure SPI
dotstar = DotStar(spi, 2) # Just one DotStar

dotstar[0] = (255, 0, 0) # Red
dotstar[1] = (255, 0, 0) # Red
time.sleep(1)
#dotstar[0] = (0, 128, 0, 0.5) # Green, half brightness
dotstar[0] = (0, 255, 0) # Green
time.sleep(1)
dotstar.fill((0,0,255)) # Blue
time.sleep(1)

def rainbow(num):
    # Rainbow Flow
    state = 0
    r = 255
    g = 0
    b = 0

    while True:
        if state == 0:
            g += 1
            if g == 255:
                state = 1

        if state == 1:
            r -= 1
            if r == 0:
                state = 2

        if state == 2:
            b += 1
            if b == 255:
                state = 3

        if state == 3:
            g -= 1
            if g == 0:
                state = 4

        if state == 4:
            r += 1
            if r == 255:
                state = 5

        if state == 5:
            b -= 1
            if b == 0:
                state = 0

        #dotstar[num] = (r, g, b)
        dotstar.fill((r, g, b))

rainbow(0)
'''