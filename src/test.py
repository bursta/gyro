from pixel_control import PixelControl
import time
from pixel_map import pixelmap
import _thread


def getConfig():
    config = {'rpm': 0, 'led': 1}

    with open('config.ini', 'r') as configfile:
        variables = configfile.readlines()

        for variable in variables:
            varSplit = variable.split('=')
            key = varSplit[0]
            value = int(varSplit[1])

            config[key] = value

    return config

def setConfig(rpm, led):
    with open('config.ini', 'w') as configfile:
        configfile.write("rpm="+str(rpm)+"\nled="+str(led))


CONFIG = getConfig()

# global variables and constants
ACTIVE = True

ANGLE = CONFIG['angle']
VELOCITY = CONFIG['rpm'] / 60 / 3 / (1000000 * 1000)   # per Second and gear ratio -> real RPS
VELOCITY_DEGREE = VELOCITY * 360
GEAR_RATIO = (90 / 29) * (60 / 20)

print(f'{VELOCITY:.12f}', f'{VELOCITY_DEGREE:.12f}', GEAR_RATIO)


# thread to handle pixel color change by rotation angle
def pixelThread():
    global ACTIVE
    global ANGLE
    global VELOCITY_DEGREE
    global GEAR_RATIO

    scanGap_us = 1000000 * 1000
    lastTick_us = time.time_ns() - scanGap_us
    
    control = PixelControl(pixelmap, gearRatio = GEAR_RATIO)

    loopCount = 1
    while True:
        if ACTIVE:
            currentTick_us = time.time_ns()
            currentScanGap_us = currentTick_us - lastTick_us
            
            if currentScanGap_us >= scanGap_us:
                print(loopCount)
                loopCount += 1
                # calc angle by expired time
                ANGLE += (VELOCITY_DEGREE * currentScanGap_us)
                colors = control.getColors(ANGLE)

                #print(' --- Angle ',ANGLE, ' Colors ', colors)
                lastTick_us = time.time_ns()


#_thread.start_new_thread(pixelThread, ())
pixelThread()
