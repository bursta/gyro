#import numpy as np
import math


class Sphere:
    """
    Calculate the position of a dot on the surface of a sphere by rotating a rotating plane.

    :param float diameter: length of the centered spinning bar in mm
    :param float layerRatio: gear ratio between first and second (inner and outer) rotation plane

    """

    radius = 135
    layerRatio = 4.8

    def __init__(self, diameter, layerRatio):
        self.radius = diameter / 2 # np.true_divide( diameter, 2 )
        self.layerRatio = layerRatio

    def getPos(self, angle):
        firstLayerAngle = angle % 360 # np.mod( angle, 360 )
        secLayerAngle = ( firstLayerAngle * self.layerRatio ) % 360 # np.mod( np.multiply( firstLayerAngle, self.layerRatio ), 360 )

        alpha = math.radians( secLayerAngle ) #  np.radians( secLayerAngle )

        z = math.cos( alpha ) * self.radius # np.multiply( np.cos( alpha ), self.radius )

        yZ = math.sin( alpha ) * self.radius # np.multiply( np.sin( alpha ), self.radius )

        beta = math.radians( angle ) # np.radians( angle )

        x = math.sin( beta ) * yZ # np.multiply( np.sin( beta ), yZ )

        y = math.cos ( beta ) * yZ # np.multiply( np.cos( beta ), yZ )

        print(f'1st {angle}°  2nd {round(secLayerAngle, 2)}°')
        print(f'      X: {round(x, 2)}  Y: {round(y, 2)}  Z: {round(z, 2)}' )
        print()
