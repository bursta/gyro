'''
possible Result:
    Big 100 Small 29
    Ratio 10.344827586206897
    Outer Angle 12.000000000000004
    Angle 55.86206896551721         124.13793103448279
'''


import math
for bgear in [90, 100]:
    for sgear in range(20, 35, 1):
        ratio = bgear / sgear
        angle = ( ( (bgear * 360 * 3) / sgear ) - (math.floor(ratio)*3 * 360) ) % 360.0
        tangle = angle
        if tangle > 180.0:
            tangle -= 180.0
        elif tangle > 90.0:
            tangle = 180.0 - tangle
        if abs(angle) not in [0.0, 180.0, 360.0]:
            print('Big', bgear, 'Small', sgear)
            print('Ratio', ratio*3)
            print('Outer Angle', angle/(ratio*3))
            print('Angle', tangle, '       ', angle)
            print()